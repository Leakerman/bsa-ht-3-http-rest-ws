export const getCurrentRoomName = (socket, rooms) => {
  return Object.keys(socket.rooms).find((roomName) => rooms.has(roomName));
};

export const roomsArr = (rooms) => {
  return Array.from(rooms)
    .map(([k, v]) => ({ name: k, ...v }))
    .filter((room) => !room.isStarted);
};

export function isAllPlayersFinished(players) {
  return players.reduce((isEveryoneFinished, player) => {
    return !!player.finishTime && isEveryoneFinished;
  }, !!players.length);
}
