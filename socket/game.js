import { SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from "./config";
import roomsHandler from "./rooms/rooms.handler";
import { roomsArr, getCurrentRoomName, isAllPlayersFinished } from "./utils";

const users = [];
const rooms = new Map();

export default (socket, io) => {
  const username = socket.handshake.query.username;
  if (users.includes(username)) {
    socket.emit("USERNAME_ALREADY_TAKEN", username);
    return;
  } else {
    users.push(username);
    socket.emit("UPDATE_ROOMS", roomsArr(rooms));
  }

  roomsHandler(socket, io, rooms, users, username);

  socket.on("USER_TYPED_SYMBOL", ({ userTyped, secondsLeft }) => {
    const roomName = getCurrentRoomName(socket, rooms);
    const roomInfo = rooms.get(roomName);
    const currPlayer = roomInfo.players.find((p) => p.username === userTyped);
    currPlayer.symbolsTyped += 1;

    
    if(currPlayer.symbolsTyped === roomInfo.text.length) {
      currPlayer.finishTime = secondsLeft
      if(isAllPlayersFinished(roomInfo.players)) {
        io.in(roomName).emit("GAME_OVER", roomInfo.players)
        return
      }
    }
    io.in(roomName).emit("UPDATE_ROOM", { name: roomName, ...roomInfo });
  });

  socket.on("TIME_OVER", () => {
    const roomName = getCurrentRoomName(socket, rooms);
    const roomInfo = rooms.get(roomName);
    socket.emit("GAME_OVER", roomInfo.players)
  })

  socket.on("SET_DEFAULT_ROOM", () => {
    const roomName = getCurrentRoomName(socket, rooms);
    const roomInfo = rooms.get(roomName);
    roomInfo.players.forEach((player) => {
      player.finishTime = 0;
      player.symbolsTyped = 0;
      player.ready = false;
    });
    roomInfo.isStarted = false;
    roomInfo.text = "";

    io.in(roomName).emit("UPDATE_ROOM", {
      name: roomName,
      ...roomInfo,
    });

    socket.emit("UPDATE_ROOMS", roomsArr(rooms));
    socket.broadcast.emit("UPDATE_ROOMS", roomsArr(rooms));
  });

  socket.on("START_TIMER", (text) => {
    const msBeforeStartGame = SECONDS_TIMER_BEFORE_START_GAME * 1000;
    let secondsBeforeStart = msBeforeStartGame;

    const roomName = getCurrentRoomName(socket, rooms);
    const roomInfo = rooms.get(roomName);
    roomInfo.text = text;

    let interval = setInterval(() => {
      socket.emit("GAME_START_IN", secondsBeforeStart / 1000);
      secondsBeforeStart -= 1000;
    }, 1000);

    setTimeout(() => {
      clearInterval(interval);
      socket.emit("START_GAME", SECONDS_FOR_GAME, {
        name: roomName,
        ...roomInfo,
      });
    }, msBeforeStartGame);
  });
};

