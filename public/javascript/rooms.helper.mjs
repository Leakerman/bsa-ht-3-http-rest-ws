import { socket } from "./game.mjs";
import { addClass, removeClass, createElement } from "./dom.helper.mjs";

const username = sessionStorage.getItem("username");

const progressContainer = document.getElementById("progress-container");
const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const gameTitle = document.querySelector("#game-page .title");
const roomsContainer = document.getElementById("rooms-container");

const readyButton = document.getElementById("ready-button");
const createRoomButton = document.getElementById("create-room");
const backToRoomsButton = document.getElementById("back-to-rooms");

readyButton.addEventListener("click", () => {
  socket.emit("USER_TOGGLE_READINESS", username);
});

createRoomButton.addEventListener("click", () => {
  socket.emit("CREATE_ROOM", prompt("Enter the room name"));
});

backToRoomsButton.addEventListener("click", () => {
  addClass(gamePage, "display-none");
  removeClass(roomsPage, "display-none");

  socket.emit("BACK_TO_ROOMS");
});

export const usernameAlreadyTaken = (username) => {
  alert(`Username ${username} is already taken`);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
};

export const createRoomFailed = (roomName) => {
  alert(`Name ${roomName} is already taken`);
};

const createRoomItem = ({ name, players }) => {
  const room = createElement({
    tagName: "div",
    className: "room",
    html: `
    <div class="room-connected">Users connected: ${players.length}</div>
    <p class="room-name margin-auto">${name}</p>
    `,
  });

  const joinButton = createElement({
    tagName: "button",
    className: "button margin-auto",
    content: "Join",
  });

  joinButton.addEventListener("click", () => {
    socket.emit("JOIN_ROOM", name);
  });

  room.append(joinButton);

  return room;
};

export const updateRooms = (rooms) => {
  const allRooms = rooms.map(createRoomItem);
  roomsContainer.innerHTML = "";
  roomsContainer.append(...allRooms);
};

export const joinRoomDone = ({ name }) => {
  addClass(roomsPage, "display-none");
  removeClass(gamePage, "display-none");

  gameTitle.innerText = name;
};

function createProgressItem(player) {
  const classes = ["progress"];

  if (player.ready) {
    classes.push("active");
  }

  if (!!player.finishTime) {
    classes.push("finished");
  }

  const playerNick =
    username === player.username ? player.username + " (you)" : player.username;

  const progressValue = !!window.textLength
    ? (player.symbolsTyped / window.textLength) * 100
    : 0;

  const html = `
      <div class="username">${playerNick}</div>
      <progress class="progressbar" value="${progressValue}" max="100"></progress>
  `;

  return createElement({
    tagName: "div",
    className: classes.join(" "),
    attributes: { id: player.username },
    html,
  });
}

export const updateRoom = ({  players }) => {
  const activePlayer = players.find((p) => p.username === username);
  activePlayer.ready
    ? (readyButton.innerText = "Not ready")
    : (readyButton.innerText = "Ready");

  progressContainer.innerHTML = "";
  const playersProgress = players.map(createProgressItem);
  progressContainer.append(...playersProgress);
};
