import { socket } from "./game.mjs";
import { addClass, removeClass, createElement } from "./dom.helper.mjs";

const username = sessionStorage.getItem("username");

const readyButton = document.getElementById("ready-button");
const timer = document.getElementById("timer");
const gameWrap = document.getElementById("text");
const gameText = document.getElementById("game-text");
const gameTimer = document.getElementById("time-left");
const backToRoomsButton = document.getElementById("back-to-rooms");
let symbols = null;

const typing = (e) => {
  if (e.repeat) return;
  const typed = e.key;
  for (let i = 0; i < symbols.length; i++) {
    const symbolToCheck =
      symbols[i].innerHTML === "&nbsp;" ? " " : symbols[i].innerHTML;

    if (symbolToCheck === typed) {
      // if typed letter is the one from the word
      if (symbols[i].classList.contains("typed")) {
        // if it already has class with the background color then check the next one
        continue;
      } else if (
        (!symbols[i].classList.contains("typed") && !symbols[i - 1]) ||
        symbols[i - 1].classList.contains("typed")
      ) {
        // if it dont have class, if it is not first letter or if the letter before it dont have class (this is done to avoid marking the letters who are not in order for being checked, for example if you have two "A"s so to avoid marking both of them if the first one is at the index 0 and second at index 5 for example)
        removeClass(symbols[i], "next");
        if (symbols[i + 1]) {
          addClass(symbols[i + 1], "next");
        }
        symbols[i].classList.add("typed");
        socket.emit("USER_TYPED_SYMBOL", {
          userTyped: username,
          secondsLeft: window.secondsLeft,
        });
        break;
      }
    }
  }
};

const displayTimer = () => {
  addClass(readyButton, "display-none");
  addClass(backToRoomsButton, "display-none");
  removeClass(timer, "display-none");
};

export const prepareTimer = (textIndex) => {
  const theUrl = `http://localhost:3002/game/texts/${textIndex}`;
  const xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", theUrl, false);
  xmlHttp.send(null);

  displayTimer();

  const text = JSON.parse(xmlHttp.responseText).text;
  socket.emit("START_TIMER", text);

  window.textLength = text.length;
  gameText.innerHTML = "";
  text.split("").forEach((symbol) => {
    const el = createElement({
      tagName: "span",
      className: "span",
      html: symbol === " " ? "&nbsp;" : symbol,
    });
    gameText.appendChild(el);
  });
  symbols = document.querySelectorAll(".span");
  addClass(symbols[0], "next");
};

export const startGame = (gameTime) => {
  addClass(timer, "display-none");
  removeClass(gameWrap, "display-none");

  window.secondsLeft = gameTime;
  document.addEventListener("keydown", typing, false);

  const interval = setInterval(() => {
    gameTimer.innerText = `seconds left ${window.secondsLeft}`;
    if (window.secondsLeft <= 0) {
      socket.emit("TIME_OVER");
      clearInterval(interval);
      gameTimer.innerText = `seconds left ${gameTime}`;
      return;
    }
    window.secondsLeft--;
  }, 1000);
  window.interval = interval;
};

export const gameOver = (players) => {
  alert(createLeaderboardFromPlayers(players));
  setDefault();
};

function setDefault() {
  window.textLength = "";
  clearInterval(window.interval);
  document.removeEventListener("keydown", typing);

  addClass(gameWrap, "display-none");
  removeClass(readyButton, "display-none");
  removeClass(backToRoomsButton, "display-none");

  socket.emit("SET_DEFAULT_ROOM");
}

function createLeaderboardFromPlayers(players) {
  const playersFinished = players
    .filter((p) => !!p.finishTime)
    .sort((a, b) => b.finishTime - a.finishTime);
  const playersNotFinished = players
    .filter((p) => !p.finishTime)
    .sort((a, b) => b.symbolsTyped - a.symbolsTyped);
  const places = [...playersFinished, ...playersNotFinished];
  return places
    .map((player, index) => `${index + 1}. ${player.username}`)
    .join("\n");
}
