import {
  updateRoom,
  joinRoomDone,
  updateRooms,
  usernameAlreadyTaken,
  createRoomFailed,
} from "./rooms.helper.mjs";
import { 
  prepareTimer,
  startGame,
  gameOver, 
} from "./game.helper.mjs";

const username = sessionStorage.getItem("username");
if (!username) {
  window.location.replace("/login");
}
export const socket = io("", { query: { username } });

const timer = document.getElementById("timer");

socket.on("PREPARE_TIMER", (textIndex) => prepareTimer(textIndex));
socket.on("GAME_START_IN", (seconds) => { timer.innerText = seconds; });
socket.on("START_GAME", (gameTime, room) => startGame(gameTime));
socket.on("GAME_OVER", (players) => gameOver(players));

socket.on("USERNAME_ALREADY_TAKEN", (username) => usernameAlreadyTaken(username));
socket.on("CREATE_ROOM_FAILED", (roomName) => createRoomFailed(roomName));
socket.on("UPDATE_ROOMS", (roomsEntries) => updateRooms(roomsEntries));
socket.on("JOIN_ROOM_DONE", (room) => joinRoomDone(room));
socket.on("UPDATE_ROOM", (room) => updateRoom(room));
socket.on("JOIN_ROOM_FAILED", () => alert("The maximum number of players is reached in the room"));
